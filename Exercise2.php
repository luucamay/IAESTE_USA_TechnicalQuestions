<?php
// Method that reverses the order of a string.
function reverseString($string){
	$length = strlen($string); 
	for($i = 0, $j = $length-1; $i < $j; $i++, $j--){
		$temp = $string[$i];
		$string[$i] = $string[$j];
		$string[$j] = $temp;
	}
	return $string;
}

// Function call to reverse with an example string.
var_dump(reverseString('Whatever you want to reverse here'));

?>
