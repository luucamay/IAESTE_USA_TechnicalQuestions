<?php
/* Method that will determine whether 
 * any string of length n has any duplicate 
 * characters within that string. */
function uniqueChars($string){
	$length = strlen($string);
	$string = strtolower($string);
	$max_chars = 256;
	/* If length is greater than 256,
       some characters must have been repeated. */
	if($length > $max_chars){
		return false;
	}
	
	$chars = array_fill(1, $max_chars, false);
	/* Review each char of the string until 
	   it founds a duplicate an returns false. */
	for($i = 0; $i < $length; $i++){
		// Gets the ascci value of the character at position $i.
		$index = ord($string[$i]);
		/* If the value is already true, string
           has duplicate characters, return false. */
		if($chars[$index] == true){
			return false;
		}
		$chars[$index] = true;
	}
	// No duplicates encountered, return true.
	return true;
}

/* Function call to determine if a string has
   duplicate characters with an example string. */
if(uniqueChars('Whatever you want to reverse here')){
	echo 'The string DOES NOT HAVE duplicate characters';
}
else{
	echo 'The string HAS duplicate characters';
}
?>
