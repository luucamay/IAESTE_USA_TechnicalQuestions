<?php
// Method to display all the prime number from 1 to n.
function printPrimes($n){
    $counter = 0;
    /* For every number between 2 to n, check 
    whether it is prime number or not. */
	for($i = 2; $i <= $n; $i++){
		$counter = 0;
		// Check whether $i is prime or not.
		for($j = 2; $j < $i; $j++){
			if($i % $j == 0){
				$counter++;
			}
		}
		if($counter == 0){
			echo $i.' ';
		}
	}
        
}

// Function call to print Prime numbers from 1 to n.
echo printPrimes(10);
?>
