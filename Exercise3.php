<?php
/* Method where given a number n will generate
 * a Fibonacci Series for n iterations. */
function printFibonacci($n){
	$first = 0;
	$second = 1;
	echo 'Fibonacci: ';
	echo $first.','.$second;
	
	for($i = 0; $i < $n; $i++){
		$third = $first + $second;
		echo ','.$third;
		$first = $second;
		$second = $third; 
    }
}
  
// Function call to print Fibonacci series upto 6 numbers.
printFibonacci(10);
?>
